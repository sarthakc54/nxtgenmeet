import React, { useState } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import ScheduleMeet from './scheduleMeet'
import Home from './home'
import JoinRoom from './joinroom.js'
const App = () => {


  return (
<main>
<Switch>
    <Route path="/schedule-start" component={ScheduleMeet} exact />
    <Route path="/joinRoom" component={JoinRoom} />
    <Route path="/" component={Home} />
</Switch>
</main>

  )
}

export default App
