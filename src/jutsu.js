import React, { useState } from 'react'
import { useHistory } from 'react-router-dom';
import { Jutsu } from 'react-jutsu'
const App = (props) => {
  const history = useHistory();
  const [room, setRoom] = useState(props.room)
  const [name, setName] = useState(props.name)
  //const [call, setCall] = useState()
  const [password, setPassword] = useState(props.password)
  const [subject, setSubject] = useState(props.subject)



  return (
<Jutsu
       containerStyles={{ width: window.innerWidth, height: window.innerHeight }}
       domain='meet.onacall.in'
       subject={subject}
        roomName={room}
        password={password}
        displayName={name}
        onMeetingEnd={() =>  history.push('/')}
        loadingComponent={<p> NxtGen Meet is loading ...</p>} />

  )
}

export default App
