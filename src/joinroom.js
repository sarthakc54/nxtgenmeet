import React, { useState,useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import Jutsu from './jutsu.js'
import {Form,Col,Button,Row} from 'react-bootstrap'


var logo = require('./NxtGen logo.png')
const JoinRoom = () => {

  const history = useHistory();

  const [room, setRoom] = useState('')
  const [name, setName] = useState('')
  const [call, setCall] = useState(false)
  const [password, setPassword] = useState('')
  //reactLocalStorage.set('serialNo', 0);




  const startNow = (event) => {

   event.preventDefault()
    if (room && name){
      setCall(true)

    }
  }

  return (
    <div style={{alignContent:"center"}}>
      <div class="topleft">
    <img src={logo} height='50px' onClick={()=> history.push('/')}  ></img>
  </div>
{call ? (<Jutsu room={room} name={name} pass={password}></Jutsu>)
        : (
          <div style={{display:"flex", flexDirection:"column"}}>

<h3 style={{alignSelf:"center", padding:"40px", margin:"40px"}}>Join Room</h3>
          <div style={{display:"flex", flexDirection:"row", alignSelf:"center"}}>
<div className="shadow-lg  rounded" >
 <Form style={{display:"flex", flexDirection:"column"}}>

    <Form.Group >
      <Form.Label>Room</Form.Label>
      <Form.Control type="text"  placeholder="Enter Room ID" value={room} onChange={(e) => setRoom(e.target.value)} />
    </Form.Group>

  <Form.Group  as={Row}>
    <Col><Form.Label>Name</Form.Label>
      <Form.Control type="text"  placeholder="Enter Your Name" value={name} onChange={(e) => setName(e.target.value)}/>
    </Col>
    <Col>
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password(Optional)" value={password} onChange={(e) => setPassword(e.target.value)}/>
  </Col>
      </Form.Group>

<Form.Group >

  <Button style={{width:"100%", padding:"10px"}} variant="success" onClick={startNow} type="submit" >
    Join
  </Button>

  </Form.Group>

</Form>

  </div>
  </div>
  </div>
        )}



     </div>

  )
}

export default JoinRoom
