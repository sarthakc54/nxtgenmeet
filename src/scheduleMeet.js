import React, { useState,useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import Jutsu from './jutsu.js'
import {Form,Col,Button,Row,InputGroup} from 'react-bootstrap'
import uuid from "uuid/v4"
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {reactLocalStorage} from 'reactjs-localstorage';
import {EmailShareButton} from 'react-share'
import Card from 'react-bootstrap/Card'
import logo from './NxtGen logo.png'
import {CopyToClipboard} from 'react-copy-to-clipboard';

const App = () => {
  useEffect(() => {
    // Update the document title using the browser API
   renderDetails();
  },[]);
  const history = useHistory();

  const [room, setRoom] = useState(uuid())
  const [name, setName] = useState('')
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [call, setCall] = useState(false)
  const [password, setPassword] = useState('')
  const [startDate, setStartDate] = useState(new Date());
  const [indents, setIndent]=useState([])
  const [copied, setCopied]=useState(false)
  //reactLocalStorage.set('serialNo', 0);

  const renderDetails=async()=>{
    const indents = [];
    for (var i = 0; i < 5; i++) {
      var details= reactLocalStorage.getObject(i)
      console.log(details)
      if(details.room!=null){
        indents.push(
        <Card style={{margin:"10px"}} className="shadow-lg  rounded">
          <Card.Header as="h5">{details.title}</Card.Header>
          <Card.Body>
          <h5>{details.startDate}</h5>
          <b >Room ID  : </b>{details.room}
          <CopyToClipboard text={details.room}
          onCopy={() => setCopied(true)}>
          <i class="fas fa-copy" style={{marginLeft:'20px'}}></i>
        </CopyToClipboard>

        <br></br>
          <b>Host Name : </b>{details.name}<br></br>
          <br></br>
        <Button style={{width:"30%"}} id={i} variant="success" onClick={handleClick}>Start </Button>
          <Button style={{width:"30%"}} id={i} variant="danger" onClick={removeMeeting}>Remove</Button>
          <EmailShareButton style={{width:"20%"}} className="btn" subject="NxtGen Meet link" body={"Room ID:"+details.room+""}> Share Details</EmailShareButton>

          </Card.Body>

          </Card>);
      }

      setIndent(indents);

      }

   // return indents;
  }
  const removeMeeting = (e) => {
  reactLocalStorage.remove(e.target.id)
  renderDetails();
   }
  const handleClick = (e) => {
    var room1 = reactLocalStorage.getObject(e.target.id).room
    var name1 = reactLocalStorage.getObject(e.target.id).name
    console.log(room1)
    console.log(name1)
   // event.preventDefault()
    if (room1 && name1){
      setCall(true)
      setName(name1)
      setRoom(room1)
    }
  }

  const startNow = (event) => {

   event.preventDefault()
    if (room && name){
      setCall(true)

    }
  }
  const createSchedule = event => {
    var serialNo= parseInt(reactLocalStorage.get('serialNo',0,true))
    const time= new Date().getTime()
    event.preventDefault()
    reactLocalStorage.setObject([serialNo], {

        'timeStamp':time,
        'title':title,
        'description':description,
        'room': room,
        'name': name,
        'password':password,
        'startDate':startDate.toLocaleString()



    });
    if(serialNo>=5){
      serialNo=0
    reactLocalStorage.set('serialNo',serialNo+1)

    }
    else{
      reactLocalStorage.set('serialNo',serialNo+1)

    }
    renderDetails();

  }
  return (
    <div  >
      <div class="topleft">
    <img src={logo} style={{maxHeight:50}}  onClick={()=> history.push('/')} ></img>
  </div>
{call ? (<Jutsu room={room} name={name} password={password} subject={title}></Jutsu>)
        : (
<div style={{padding:"7%"}}>

<h3 style={{marginLeft:"40%"}}> Schedule Call</h3>
          <div  className="divSchedule"  >

<div >
 <Form >

    <Form.Group >
      <Form.Label>Room</Form.Label>

      <InputGroup className="mb-2">
      <Form.Control type="text"  placeholder="Enter Room ID" value={room} disabled/>
        <InputGroup.Append>
        <CopyToClipboard text={room}
          onCopy={() => setCopied(true)}>
          <i class="fas fa-copy" style={{marginLeft:10, marginTop:10}}></i>
        </CopyToClipboard>
        </InputGroup.Append>

      </InputGroup>


    </Form.Group>

    <Form.Group  >
      <Form.Label>Meeting Subject</Form.Label>
      <Form.Control type="text"  placeholder="Enter Meeting Title" value={title} onChange={(e) => setTitle(e.target.value)}/>
    </Form.Group>
  <Form.Group  as={Row}>
    <Col><Form.Label> Name</Form.Label>
      <Form.Control type="text"  placeholder="Enter Your Name" value={name} onChange={(e) => setName(e.target.value)}/>
    </Col>
    <Col>
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password(Optional)" value={password} onChange={(e) => setPassword(e.target.value)}/>
  </Col>
      </Form.Group>
    <Form.Group  >
      <Form.Label>Meeting Discription</Form.Label>
      <Form.Control as="textarea" rows={3}  placeholder="Enter Meeting Description" value={description} onChange={(e) => setDescription(e.target.value)}/>
    </Form.Group>
  <Form.Group >
    <Form.Label>Time</Form.Label>
    <br></br>
    <DatePicker className="form-control" selected={startDate} onChange={date => setStartDate(date)} showTimeSelect dateFormat="MMMM d-h:mm aa"
      />
  </Form.Group>
<Form.Group as={Row}>
<Col>
  <Button variant="primary" onClick={createSchedule} type="submit" >
    Schedule
  </Button>
  </Col>
  <Col>
  <Button variant="success" onClick={startNow} type="submit" >
    Start Now
  </Button>
  </Col>
  </Form.Group>
  {copied ? alert("RoomID Copied") : null}
</Form>

  </div>
  <div className="scheduleDiv" >
          {indents}
        </div>

  </div>
  </div>
        )}



     </div>

  )
}

export default App
