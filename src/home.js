import React, { useState } from 'react'
import { useHistory } from 'react-router-dom';
import {Form,Col,Button,Row} from 'react-bootstrap'
import logo from './NxtGen logo.png'
import meet from './video chat.jpg'
const Home = (props) => {
  const history = useHistory();



  return (
<div style={{display:"flex"}}>
<div class="topleft">
    <img src={logo} height='50px' ></img>
  </div>
<div  class="middle" style={{display:"flex", flexDirection:"column" }}>

  <div  >
<div >
    <h3 style={{padding:"30px"}} >Join and Host High Quality Video calls with NxtGen Meet </h3>

    <Form.Group as={Row}>
<Col>
  <Button variant="primary" onClick={()=> history.push('/schedule-start')}  type="submit" >
    Start
  </Button>
  </Col>
  <Col>
  <Button variant="success" onClick={()=> history.push('/joinRoom')} type="submit" >
    Join
  </Button>
  </Col>
  </Form.Group>


  </div>
  <div><img className="img_home" src={meet} ></img></div>

  </div>

</div>
<div class="bottomleft">
    <p>CopyRights @ NxtGen 2021</p>
  </div>
</div>

  )
}

export default Home
